'use strict';

const _ = require('lodash');
const response = require('./response.js');

const levels = {
    info: 0,
    debug: 1,
    error: 2,
};

const errors = {
    _generic: {
        level: 'error',
        msg: 'Oh snap, something bad happened. Please try again'
    },
    invalid_location: {
        level: 'info',
        msg: "Sorry, I don't recognize that location",
    },
    country_location: {
        level: 'info',
        msg: 'Please specify a city, neighbourhood, or place of interest',
    },
    hebcal_tz_error: {
        level: 'info',
        msg: "Sorry, couldn't find that timezone in our database",
    },
};


exports.errorMsg = function (e, opts) {
    let msg;
    if (_.includes(_.keys(errors), e)) {
        msg = errors[e].msg;
        if (levels[errors[e].level] > 0) {
            console.error(e);
            if (opts.hasOwnProperty('req')) {console.error(JSON.stringify(opts.req.body, null, 2));}
        }
    }
    else {
        console.error(e);
        if (opts.hasOwnProperty('req')) {console.error(JSON.stringify(opts.req.body, null, 2));}
        msg = errors._generic.msg;
    }

    if (opts.hasOwnProperty('msg')) {msg = opts.msg;}

    return response.makeSimpleReponse(msg, msg, true)
};

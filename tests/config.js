'use strict';

const _ = require('lodash');

try {
    const config = require('../config.json');
    _.forOwn(config, function(value, key) {
        process.env[key] = value;
    });
} catch (e) {}

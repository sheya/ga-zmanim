'use strict';

const request = require('request');
const utils = require('../utils.js');
const config = require('./config.js');
const actions = require('../actions');

const googleMapsClient = require('@google/maps').createClient({
    key: process.env.GOOGLE_MAPS_KEY
});


function getGeoFromLocation (location) {
    return new Promise((resolve, reject) => {
        googleMapsClient.geocode({
            address: location
        }, function (err, response) {
            if (!err) {
                const data = response.json.results[0];
                if (data) {
                    resolve({
                        lat: data.geometry.location.lat,
                        lng: data.geometry.location.lng,
                        address_level_1: data.address_components[0].long_name,
                        address_level_2: utils.getAttr(data, 'address_components[1].long_name'),
                        formatted_address: data.formatted_address,
                    });
                }
                else {reject('invalid_location');}
            }
        })
    });
}

function getTimezoneForCoords (lat, lng) {
    let ts = Math.round((new Date()).getTime() / 1000);
    return new Promise((resolve, reject) => {
        googleMapsClient.timezone({
            timestamp: ts,
            location: lat + ',' + lng,
        }, function (err, response) {
            if (!err) {
                resolve(response.json)
            }
            else {reject('An error has occurred');}
        });
    });
}


function getLocationGeo (location) {
    return new Promise((resolve, reject) => {
        getGeoFromLocation(location).then(function (geo) {
            getTimezoneForCoords(geo.lat, geo.lng).then(function (tz) {
                geo.tz = tz;
                resolve(geo)
            });
        }, function (e) {reject(e)});
    });
}

getLocationGeo('India').then(function(x){});
//getTimezoneForCoords(20.593684, 78.96288)
'use strict';

const request = require('request');
const actions = require('../actions');


exports.standalone = function (location) {
    const params = {
        location: {city: location || 'London'}
    };

    console.time('get_candlelighting standalone');
    actions.get_candlelighting(params).then(function(result) {
        console.timeEnd('get_candlelighting standalone');
        console.log(result);
    });
};


exports.withServer = function (location) {
    const body = require('./payload.json');
    if (location) {body.result.parameters.location = location;}
    const reqOpts = {
        json: body,
        method: 'POST',
        uri: 'http://localhost:' + String(process.env.PORT) + '/ga-zmanim',
    };

    console.time('get_candlelighting with server');
    request(reqOpts, function (err, resp, body) {
        console.timeEnd('get_candlelighting with server');
        console.log(body)
    });
};

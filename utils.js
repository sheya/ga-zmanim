'use strict';


exports.getAttr = function (obj, attr, def) {
    try { return eval('obj.' + attr); }
    catch (e) { return def; }
};

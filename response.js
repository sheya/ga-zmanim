'use strict';


exports.makeSimpleReponse = function (textToSpeech, displayText, expectUserResponse) {
    displayText = displayText || textToSpeech;
    expectUserResponse = expectUserResponse || false;

    return {
        payload: {
            google: {
                expectUserResponse: expectUserResponse,
                richResponse: {
                    items: [
                        {
                            simpleResponse: {
                                displayText: displayText,
                                textToSpeech: textToSpeech,
                            }
                        }
                    ]
                }
            }
        },
        telegram: {
            text: displayText,
            parse_mode: "markdown"
        },
        facebook: {
            text: displayText
        }
    };
};

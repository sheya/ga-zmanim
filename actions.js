'use strict';

const candles = require('./candles');


exports.welcome = function (params) {
    const msg = 'Hi! Try asking "When is shabbos in Stamford Hill?"';
    return {
        textToSpeech: msg,
        displayText: msg,
    }
};


exports.get_candlelighting = function (params) {
    return new Promise((resolve, reject) => {
        candles.getCandles(params.location).then(function(reply) {
            resolve({
                textToSpeech: reply.speech,
                displayText: reply.text,
            });
        }, function (e) {reject(e)});
    });
};

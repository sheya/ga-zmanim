'use strict';

const _ = require('lodash');
const errors = require('./errors');
const request = require('request');
const utils = require('./utils.js');
const striptags = require('striptags');
const moment = require('moment-timezone');


const googleMapsClient = require('@google/maps').createClient({
    key: process.env.GOOGLE_MAPS_KEY
});


String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};


function getGeoFromLocation (location) {
    return new Promise((resolve, reject) => {
        googleMapsClient.geocode({
            address: location
        }, function (err, response) {
            if (!err) {
                const data = response.json.results[0];
                if (data) {
                    resolve({
                        lat: data.geometry.location.lat,
                        lng: data.geometry.location.lng,
                        address_level_1: data.address_components[0].long_name,
                        address_level_2: utils.getAttr(data, 'address_components[1].long_name'),
                        formatted_address: data.formatted_address,
                    });
                }
                else {reject('invalid_location');}
            }
        })
    });
}


function getTimezoneForCoords (lat, lng) {
    let ts = Math.round((new Date()).getTime() / 1000);
    return new Promise((resolve, reject) => {
        googleMapsClient.timezone({
            timestamp: ts,
            location: lat + ',' + lng,
        }, function (err, response) {
            if (!err) {
                resolve(response.json)
            }
            else {reject('An error has occurred');}
        });
    });
}


function getLocationGeo (location) {
    return new Promise((resolve, reject) => {
        getGeoFromLocation(location).then(function (geo) {
            getTimezoneForCoords(geo.lat, geo.lng).then(function (tz) {
                geo.tz = tz;
                resolve(geo)
            });
        }, function (e) {reject(e)});
    });
}


function makeReply (parashat, candlelighting, havdalah, geo) {
    let time;
    let r = {
        text: '',
        speech: '',
    };

    if (!candlelighting && havdalah) {
        r.speech = 'havdalah (50 min) is at';
        time = havdalah.substr(havdalah.indexOf(':') + 2)
    }
    else if (candlelighting.day() === 5 && parashat) {
        r.speech = 'shabbos starts at';
        time = candlelighting.format('LT');
    } else {
        r.speech = 'candle lighting is at';
        time = candlelighting.format('LT');
    }

    let place = geo.address_level_1;
    if (geo.address_level_1 !== geo.address_level_2) {
        place += `, ${geo.address_level_2}`;
    }

    r.speech += ` ${time} in ${place}`;
    r.text = r.speech;

    if (parashat) {
        r.text = `shabbos ${parashat} ${r.text.replace('shabbos ', '')}`;
    }

    r.text = r.text.capitalize();
    return r;
}


exports.getCandles = function (location) {
    return new Promise((resolve, reject) => {
        getLocationGeo(location).then(function (geo) {
            const url = 'https://www.hebcal.com/shabbat/?cfg=json&geo=pos&latitude=' + geo.lat + '&longitude=' + geo.lng + '&tzid=' + geo.tz.timeZoneId;
            request(url, function (err, res, data) {
                if (err) { reject(err) }
                else {
                    let items = {};
                    const hebcal = JSON.parse(data);

                    if (_.keys(hebcal)[0] === 'error') {
                       return resolve(errors.errorMsg('hebcal_tz_error', {msg: striptags(hebcal.error)}))
                    }

                    hebcal.items.forEach(function (e) {
                        let cat = e.category;

                        if (items.hasOwnProperty(cat)) {
                            items[cat].push(e);
                        } else {
                            items[cat] = [e];
                        }
                    });

                    let candlelighting, parashat, havdalah;
                    try {
                        candlelighting = moment(items.candles[0].date).tz(geo.tz.timeZoneId);
                    } catch (e) {}
                    try {
                        parashat = items.parashat[0].title;
                    } catch (e) {}
                    try {
                        havdalah = items.havdalah[0].title;
                    } catch (e) {}
                    resolve(makeReply(parashat, candlelighting, havdalah, geo))
                }
            });
        }, function (e) {reject(e)});
    });
};

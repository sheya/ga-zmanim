'use strict';

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');

try {
    const config = require('./config.json');
    _.forOwn(config, function(value, key) {
        process.env[key] = value;
    });
} catch (e) {}

const app = express();
const errors = require('./errors.js');
const actions = require('./actions.js');
const response = require('./response.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('port', (process.env.PORT || 5000));

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});


const actionsMap = {
    null: actions.welcome,
    'get_candlelighting': actions.get_candlelighting,
};


app.post('/ga-zmanim', (req, res) => {
    if (!req.body.hasOwnProperty('session') || !req.body.session.startsWith('projects/ga-zmanim-609ed/agent')) {
        res.status(403).end();
    } else {
        try {
            const action = req.body.queryResult.action,
                  params = req.body.queryResult.parameters;

            actionsMap[action](params).then(function (x) {
                res.status(200).send(response.makeSimpleReponse(x.textToSpeech, x.displayText));
            }, function (e) {
                res.status(200).send(errors.errorMsg(e, req));
            });
        } catch (e) {
            res.status(200).send(errors.errorMsg(e, req));
        }
    }
});
